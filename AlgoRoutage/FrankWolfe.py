from sympy.abc import c, f
from sympy import *
from scipy.optimize import fmin
import copy

from AlgoRoutage.Dijkstra import dijkstra


# Fonction objectif qui définit le coût de routage à minimiser
def fonction_cout(cout, flot):
    return cout * flot ** 2


# Fonction qui renvoie le coût de routage pour faire passer le flot sur les liens sélectionnés
def cout_routage(liste_liens, liste_flots):
    cout_routing = 0
    for i in range(len(liste_liens)):
        cout_routing += fonction_cout(liste_liens[i].cout, liste_flots[i])
    return cout_routing


# Fonction qui renvoie les poids nouveaux poids en calculant la dérivée de la fonction objectif évaluée au flot actuel
def mise_a_jour_poids(liste_liens, x):
    fct = fonction_cout(c, f)
    # Dérive la fonction objectif
    prim = fct.diff(f)
    fct_prime = lambdify([f, c], prim, "numpy")
    poids = [0] * len(liste_liens)
    for i in range(len(liste_liens)):
        poids[i] = fct_prime(x[i], liste_liens[i].cout)
    return poids


# Fonction qui vérifie si l'amélioration du coût est inférieure au seuil prédéfini
def critere_arret_verifie(cout_precedent, cout_actuel):
    seuil = 0.01
    return cout_precedent - cout_actuel < seuil


# Fonction qui met à jour la fonction objectif
def fonction(alpha, x, x_barre, liste_liens):
    if 1 >= alpha >= 0:
        x_sortie = list(x)
        for i in range(len(x)):
            x_sortie[i] += alpha*(x_barre[i] - x[i])
        return cout_routage(liste_liens, x_sortie)
    else:
        return 10000000


# Fonction qui permet de trouver le pas (alpha) optimal
def trouver_alpha(x, x_barre, liste_liens):
    alpha = fmin(fonction, 0, args=(x, x_barre, liste_liens))
    return alpha[0]


# Fonction qui copie la topologie d'origine en remplacant les id afin de pouvoir modifier
# les poids sans altérer le graph d'origine
def creation_nouveau_graphe(liste_routeurs, liste_liens):
    liste_routeurs_bis = copy.deepcopy(liste_routeurs)
    liste_liens_bis = copy.deepcopy(liste_liens)

    # Met à jour les nouveaux id des routeurs et des liens en leur attribuant l'opposé de l'id du réseau initial
    for routeur in liste_routeurs_bis:
        routeur.id *= -1
        for liens in routeur.liens:
            liens.id *= -1
    for liens_bis in liste_liens_bis:
        liens_bis.id *= -1
        liens_bis.routeurs[0].id *= -1
        liens_bis.routeurs[1].id *= -1

    # Fait le link entre les liens et les routeurs
    for i in range(len(liste_liens)):
        for k in range(len(liste_routeurs)):
            for j in range(len(liste_routeurs[k].liens)):
                if liste_routeurs[k].liens[j] == liste_liens[i]:
                    liste_routeurs_bis[k].liens[j] = liste_liens_bis[i]
            for j in range(len(liste_liens[i].routeurs)):
                if liste_liens[i].routeurs[j] == liste_routeurs[k]:
                    liste_liens_bis[i].routeurs[j] = liste_routeurs_bis[k]

    return liste_routeurs_bis, liste_liens_bis


# Permet de retrouver les routeurs de départ et d'arrivée dans le graph copié
def recherche_routeur(liste_routeurs, identifiant):
    for routeur in liste_routeurs:
        if routeur.id == identifiant:
            return routeur
    return None


# Algorithme de Frank-Wolfe qui permet d'optimiser la répartition du flot sur chaque lien du résea
# Retourne la liste des liens avec le flot attribué à chacun
def frankwolfe(routeur_depart, routeur_arrivee, liste_liens, liste_routeurs):
    # Calcul le plus court chemin
    pcc = dijkstra(routeur_depart, routeur_arrivee, False, 0)
    x = [(1 if i in pcc[1] else 0) for i in liste_liens]
    cout_actuel = cout_routage(liste_liens, x)
    # Initialisation du coût préçédent afin de rentrer dans la boucle
    cout_precedent = cout_actuel + 1000
    # Duplication du graphe (creation d'un graphe identique, avec comme identifants de liens
    # et routeurs les opposés des identifiants de notre graphe)
    (liste_routeurs_bis, liste_liens_bis) = creation_nouveau_graphe(liste_routeurs, liste_liens)
    routeur_depart_bis = recherche_routeur(liste_routeurs_bis, -routeur_depart.id)
    routeur_arrivee_bis = recherche_routeur(liste_routeurs_bis, -routeur_arrivee.id)

    # Reste dans la boucle tant que le critère d'arrêt n'est pas atteint
    while not critere_arret_verifie(cout_precedent, cout_actuel):
        # Mise à jour des poids
        poids = mise_a_jour_poids(liste_liens, x)
        for i in range(len(liste_liens_bis)):
            liste_liens_bis[i].cout = poids[i]

        # Trouve le plus court chemin en utilisant les nouveaux poids
        pcc = dijkstra(routeur_depart_bis, routeur_arrivee_bis, False, 0)
        # Met à jour x_barre
        x_barre = [(1 if i in pcc[1] else 0) for i in liste_liens_bis]
        alpha = trouver_alpha(x, x_barre, liste_liens)
        # Met à jour x
        for i in range(len(x)):
            x[i] += alpha*(x_barre[i] - x[i])

        cout_precedent = cout_actuel
        cout_actuel = cout_routage(liste_liens, x)

    liste_liens_sortie = copy.deepcopy(liste_liens)
    for i in range(len(liste_liens_sortie)):
        liste_liens_sortie[i].cout = x[i]

    return liste_liens_sortie
