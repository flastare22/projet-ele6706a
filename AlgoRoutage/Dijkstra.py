def dijkstra(routeur_depart, routeur_arrivee, arbre_entier, nb_routeurs):
    # On fait la liste des routeurs déjà visiter ainsi que le cout pour y arrivée et le routeur précédent
    liste_routeurs_visites = [[routeur_depart, 0, None]]
    # On fait une liste simple des routeurs visités
    liste_routeurs_simple = [routeur_depart]
    # On fait la liste des liens visités
    liste_liens_visites = []
    routeur_fin = None
    n = 0
    cout_total = 0
    # Tant qu'on n'est pas arrivé au routeur final ou que l'on a pas recouvert l'ensemble de l'arbre
    while not (routeur_fin == routeur_arrivee and not arbre_entier) and not (arbre_entier and n == nb_routeurs-1):
        liste_liens = []
        # On recupère l'ensemble des liens attachés aux routeurs déjà visités
        for routeur in liste_routeurs_visites:
            liste_liens += routeur[0].liens
        min_temp = -1
        save_routeur = None
        routeur_precedent = None
        lien_select = None
        # On parcoure l'ensemble des liens possibles
        for lien in liste_liens:
            # Si un lien ne relie pas deux routeurs déjà couverts
            if not (lien.routeurs[0] in liste_routeurs_simple and lien.routeurs[1] in liste_routeurs_simple):
                for routeur in liste_routeurs_visites:
                    if routeur[0] == lien.routeurs[0] or routeur[0] == lien.routeurs[1]:
                        # On vient récupérer le cout du chemin entre le routeur de départ et le routeur déjà
                        # decouvert du lien étudier et on l'aditionne au cout du lien étudié.
                        cout = routeur[1] + lien.cout
                        # Si le cout du chemin est infèrieur aux resultats précédent, on le sauvegarde
                        if min_temp < 0 or cout < min_temp:
                            lien_select = lien
                            min_temp = cout
                            if routeur[0] == lien.routeurs[0]:
                                save_routeur = lien.routeurs[1]
                                routeur_precedent = lien.routeurs[0]
                            else:
                                save_routeur = lien.routeurs[0]
                                routeur_precedent = lien.routeurs[1]
        # On ajoute le routeur non découvert avec le chemin de plus faible cout à la liste des routeurs couverts
        liste_routeurs_simple.append(save_routeur)
        liste_routeurs_visites.append([save_routeur, min_temp, routeur_precedent])
        # On ajoute le lien nouvellement séléctionné à la liste des liens choisis
        liste_liens_visites.append(lien_select)
        # On ajoute la valeur du lien au cout total de l'arbre actuel
        cout_total += lien_select.cout
        routeur_fin = save_routeur
        n += 1
    # Si on souhaite l'arbre des plus cours chemin entre un routeur et tous les autres alors on retourne
    if arbre_entier:
        return liste_liens_visites, liste_liens_visites, cout_total
    else:
        # Sinon, il faut remonter du routeur d'arrivé vers le routeur de départ pour récupérer le chemin. Pour
        # l'instant on a un arbre qui peut avoir plusieurs branches
        liste_liens_pcc = []
        routeur_actuel = routeur_arrivee
        cout = 0
        # On remonte la liste des routeurs séléctionnées a l'aide de la case routeur précédent
        while not routeur_actuel == routeur_depart:
            for lien in liste_routeurs_visites:
                if lien[0] == routeur_actuel:
                    for lien_select in routeur_actuel.liens:
                        if lien[2] in lien_select.routeurs:
                            liste_liens_pcc.append(lien_select)
                            cout += lien_select.cout
                            routeur_actuel = lien[2]
        # On retourne la liste des liens visités ainsi que les liens du plus court chemin et le cout de ce pcc
        return liste_liens_visites, liste_liens_pcc, cout