def dijkstra(routeur_depart, routeur_arrivee, d_lambda, c_lambda):
    # On stocke dans cette liste l'ensemble des routeurs déjà visités avec [routeur, c_lambda_cumulé ,delai , cout ,routeur précédent]
    liste_routeurs_visites = [[routeur_depart, 0, 0, 0, None]]
    # Liste des routeurs déjà visités
    liste_routeurs_simple = [routeur_depart]
    liste_liens_visites = []
    routeur_fin = None
    while not routeur_fin == routeur_arrivee:
        liste_liens = []
        # On recupère les liens attachés aux routeurs déjà sélectionnés
        for routeur in liste_routeurs_visites:
            liste_liens += routeur[0].liens
        min_temp = -1
        delai_temp = -1
        cout_temp = -1
        save_routeur = None
        lien_select = None
        for lien in liste_liens:
            # Si le lien ne relie pas deux routeurs ayant déjà été sélectionnés
            if not (lien.routeurs[0] in liste_routeurs_simple and lien.routeurs[1] in liste_routeurs_simple):
                for routeur in liste_routeurs_visites:
                    if routeur[0] == lien.routeurs[0] or routeur[0] == lien.routeurs[1]:
                        # On calcule la nouvelle valeur de c_lambda_cumulé pour le lien séléctionné
                        cout = routeur[1] + c_lambda * lien.cout + d_lambda * lien.delai
                        # Si le c_lambda_cumulé est inférieur au c_lamda_cumulé des autres liens non séléctionnés on sauvegarde le lien
                        if min_temp < 0 or cout < min_temp:
                            lien_select = lien
                            min_temp = cout
                            # On sauvegarde le routeur qui nous permet d'avoir le c_lambda_cumulé le plus faible
                            if routeur[0] == lien.routeurs[0]:
                                save_routeur = lien.routeurs[1]
                            else:
                                save_routeur = lien.routeurs[0]
                            # On sauvegarde également le delai_cumulé au niveau de ce routeur
                            delai_temp = routeur[2] + lien_select.delai
                            # Ainsi que le cout cumulé
                            cout_temp = routeur[3] + lien_select.cout
                            previous_routeur = routeur[0]

        liste_routeurs_simple.append(save_routeur)
        liste_routeurs_visites.append([save_routeur, min_temp, delai_temp, cout_temp, previous_routeur])
        liste_liens_visites.append(lien_select)
        routeur_fin = save_routeur

    # On crée une liste vide qui contenir les routeurs du plus court chemin
    liste_pcc = []
    cout_cumul = 0
    delay_cumul = 0
    routeur_actuel = routeur_arrivee
    while not routeur_actuel == routeur_depart:
        for lien in liste_routeurs_visites:
            if lien[0] == routeur_actuel:
                for lien_select in routeur_actuel.liens:
                    if lien[4] in lien_select.routeurs:
                        liste_pcc.append(lien_select)
                        cout_cumul += lien_select.cout
                        delay_cumul += lien_select.delai
                        routeur_actuel = lien[4]
    return liste_pcc, delay_cumul, cout_cumul


def dijkstra_contrainte(routeur_depart, routeur_arrivee, delai_min, n):
    #On applique dijkstra pour recuperer le plus court chemin pour aller de routeur_depart à routeur_arrivee ainsi que le cout et le delai cumulé associé
    pc, d1, c1 = dijkstra(routeur_depart, routeur_arrivee, 0, 1)
    #Si on respecte la contrainte de délai
    if d1 <= delai_min:
        return pc, d1, c1
    else:
        pd, d2, c2 = dijkstra(routeur_depart, routeur_arrivee, 1, 0)
        if d2 >= delai_min:
            return ["Impossible"],-1,-1
        else:
            for i in range(n):
                new_lambda = (c1 - c2) / (d2 - d1)
                r, dr, cr = dijkstra(routeur_depart, routeur_arrivee, new_lambda, 1)
                if (cr + new_lambda * dr) == (c1 + new_lambda * d1):
                    return pd, d2, c2
                else:
                    if dr < delai_min:
                        pd = r
                        d2 = dr
                        c2 = cr
                    else:
                        pc = r
                        d1 = dr
                        c1 = cr
            return ["Pas assez d'itérations"],-1,-1
