def prim(nb_routeur, routeur_depart):
    liste_routeurs_visites = [routeur_depart]
    liste_liens_visites = []
    nb_routeur_couverts = 1
    cout = 0
    #Tant qu'on n'a pas couvert l'ensemble des routeurs
    while not nb_routeur_couverts == nb_routeur:
        liste_liens_possibles = []
        # On recupère l'ensemble des liens attachés aux routeurs déjà visités
        for routeur in liste_routeurs_visites:
            liste_liens_possibles += routeur.liens
        lien_select = None
        # On parcoure l'ensemble des liens possibles
        for lien in liste_liens_possibles:
            # Si un lien ne relie pas deux routeurs déjà couverts
            if not (lien.routeurs[0] in liste_routeurs_visites and lien.routeurs[1] in liste_routeurs_visites):
                if lien_select is not None:
                    # Si le cout du lien est inférieur au cout du lien précédment séléctionné, on le sauvegarde
                    if lien_select.cout > lien.cout:
                        lien_select = lien
                else:
                    # Si aucun line n'a encore été séléctionné, je sauvegarde le premier lien qui ne fait pas de boucle
                    lien_select = lien
        # On sauvegarde le lien le moins cher ne faisant aucune boucle entre des routeurs déjà couverts
        liste_liens_visites.append(lien_select)
        cout += lien_select.cout
        # On ajoute le nouveau routeur séléctionné à la liste des routeurs couverts
        if lien_select.routeurs[0] in liste_routeurs_visites:
            liste_routeurs_visites.append(lien_select.routeurs[1])
        else:
            liste_routeurs_visites.append(lien_select.routeurs[0])
        # On a couvert un routeur suplementaire
        nb_routeur_couverts += 1
    #On retourne la liste des liens visités ainsi que le cout de l'arbre de recouvrement minimum
    return liste_liens_visites, cout