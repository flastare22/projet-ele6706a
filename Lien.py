class Lien:

    def __init__(self, id, routeur_a, routeur_b, cout, delai):
        self.id = id
        self.routeurs = [routeur_a, routeur_b]
        self.cout = cout
        self.delai = delai
        self.actif = True

    def __str__(self):
        return str("Lien {}:\n"
                   "    -Routeurs: {} {}\n"
                   "    -Cout: {}\n".format(self.id, self.routeurs[0], self.routeurs[1], self.cout))
