from random import randint
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox

from Routeur import Routeur
from Lien import Lien
from Graphe import creation_graphe
from AlgoRoutage.Dijkstra import dijkstra
from AlgoRoutage.Prim import prim
from AlgoRoutage.FrankWolfe import frankwolfe
from AlgoRoutage.DijkstraContrainte import dijkstra_contrainte

"""Classe permettant l'affichage des différents résultats"""


class Interface(Frame):

    def __init__(self):
        Frame.__init__(self, width=1100, height=900)
        self.canvas = Canvas(self, width=1100, height=600)
        self.canvas.bind("<Button-1>", self.callback)
        self.canvas.pack()

        self.nb_routeurs = 30
        self.liste_liens = []
        self.liste_routeurs = []
        self.routeur_depart_selected = False  # booleen indiquant si un routeur de depart à été choisi
        self.routeur_depart = None  # routeur de routeur_depart_selected choisi
        self.routeur_arrivee_selected = False  # booleen indiquant si un routeur d'arrivé à été choisi
        self.routeur_arrivee = None  # routeur d'arrivé choisi
        self.error = False
        self.affiche_delai = False
        self.delai_max = 10000
        self.folder_path = StringVar()  # chemin vers le fichier à importer
        self.liste_liens_FrankWolfe = []  # liste des routeurs choisis par Franck and Wolfe
        self.liste_liens_visites = []  # liste des liens parcourus par un algorithme
        self.liste_liens_pcc = []  # liste des routeurs du plus court chemin
        self.liste_liens_desactives = []
        self.liste_routeurs_creation_lien = []
        self.algorithme_select = 1  # indicateur de l'algorithme choisi parmis 4
        self.cout = 0  # cout du chemin affiché à l'écran
        self.delai = 0
        self.lien_select = None
        self.mode_creation_lien = False
        self.point = [0, 0]

        self.nouveau_graphe()

        menu = Frame()
        menu.pack(side=TOP)

        start = Button(menu, text="Start", command=self.start)
        start.pack(side=LEFT)

        new_graphe = Button(menu, text="Générer nouveau graphe", command=self.nouveau_graphe)
        new_graphe.pack(side=LEFT)

        algorithme = StringVar()
        algorithme.set("Arbre de recouvrement minimum")

        liste_algorithmes = ["Arbre de recouvrement minimum", "Dijkstra", "Frank_Wolfe", "Dijktra avec contraintes"]

        optionMenu = OptionMenu(menu, algorithme, *liste_algorithmes, command=self.choix_algorithme)
        optionMenu.config(width=30)
        optionMenu.pack(side=LEFT)

        import_btn = Button(menu, text="Importer", command=self.importer)
        import_btn.pack(side=LEFT)

        export_btn = Button(menu, text="Exporter", command=self.exporter)
        export_btn.pack(side=LEFT)

        self.slider_nb_routeurs = Scale(menu, orient=HORIZONTAL, length=300, width=20, sliderlength=10, from_=10, to=60,
                                        tickinterval=5, command=self.setNbRouteur)
        self.slider_nb_routeurs.set(self.nb_routeurs)
        self.slider_nb_routeurs.pack(side=LEFT)

        var = StringVar(menu)
        var.set(str(self.delai_max))
        self.slider_delay_max = Spinbox(menu, from_=500, to=100000, command=self.setDelaiMax, textvariable=var)
        self.slider_delay_max.pack(side=LEFT)

        creation_lien_button = Button(menu,text="Nouveau Lien", command=self.creation_lien)
        creation_lien_button.pack(side=LEFT)

        menu_lien = Frame()
        menu_lien.pack(side=TOP)

        self.label_lien = LabelFrame(menu_lien, text="Aucun lien séléctionné")
        self.label_lien.pack()

        label_capacite = Label(self.label_lien, text="Capacité du lien")
        label_capacite.pack(side=LEFT)

        self.var_capacite = StringVar()
        self.var_capacite.trace("w", self.setCapacite)
        self.entry_capacite = Entry(self.label_lien, textvariable=self.var_capacite, validatecommand=self.setCapacite)
        self.entry_capacite.pack(side=LEFT)

        label_delai = Label(self.label_lien, text="Delai du lien")
        label_delai.pack(side=LEFT)

        self.var_delai = StringVar()
        self.var_delai.trace("w", self.setDelai)
        self.entry_delai = Entry(self.label_lien, textvariable=self.var_delai, validatecommand=self.setDelai)
        self.entry_delai.pack(side=LEFT)

        self.actif = IntVar(value=1)
        self.checkbox_active = Checkbutton(self.label_lien, text='Actif', variable=self.actif, onvalue=1, offvalue=0,
                                           command=self.setActive)
        self.checkbox_active.pack()

        self.pack(fill=BOTH)

        self.mainloop()

    """Fonction permettant de rafraichir l'affiche de l'écran et notamment afficher les resultats obtenus avec les 
    differnts algorithmes """

    def print(self):

        """Suppression de l'ensemble des dessins sur le Canvas"""
        self.canvas.delete("all")

        """Dessine le graph de Franck and Wolfe """
        if not len(self.liste_liens_FrankWolfe) == 0:
            for lien in self.liste_liens_FrankWolfe:
                bg = "#%02x%02x%02x" % (int(255 * lien.cout) + int(255 * (1 - lien.cout)), int(255 * (1 - lien.cout)),
                                        int(255 * (1 - lien.cout)))
                self.canvas.create_line(lien.routeurs[0].x + 50, lien.routeurs[0].y + 50, lien.routeurs[1].x + 50,
                                        lien.routeurs[1].y + 50, fill=bg, width=2)
            for lien in self.liste_liens_FrankWolfe:
                cout = int(lien.cout * 100)
                min_x = min(lien.routeurs[0].x + 50, lien.routeurs[1].x + 50)
                min_y = min(lien.routeurs[0].y + 50, lien.routeurs[1].y + 50)
                x_middle = min_x + abs(lien.routeurs[0].x - lien.routeurs[1].x) / 2
                y_middle = min_y + abs(lien.routeurs[0].y - lien.routeurs[1].y) / 2
                self.canvas.create_text(x_middle, y_middle, fill="blue", font="Times 10 italic bold",
                                        text=cout)
        else:
            """Dessine des liens du graphe princiale avec affichage du cout a mis chemin du lien"""
            for lien in self.liste_liens:
                self.canvas.create_line(lien.routeurs[0].x + 50, lien.routeurs[0].y + 50, lien.routeurs[1].x + 50,
                                        lien.routeurs[1].y + 50)
            self.print_liens(self.liste_liens, "darkblue")

        """Dessine en orange des liens qui ont été parcourue lors de l'execution d'un algorithme. Attention ce la 
        correspond pas forcement au lien choisi. Par exemple dans l'algorithme du plus court chemin, certains sommets 
        visités ne vont pas faire partie des routeurs reenus pour le plus cours chemins: leur liens appraitront alors 
        en orange """
        if not len(self.liste_liens_visites) == 0:
            for lien in self.liste_liens_visites:
                self.canvas.create_line(lien.routeurs[0].x + 50, lien.routeurs[0].y + 50, lien.routeurs[1].x + 50,
                                        lien.routeurs[1].y + 50, fill="orange", width=4)
            self.print_liens(self.liste_liens_visites, "darkblue")

        """Dessine en rouge les liens du plus cours chemin entre une source et une destination"""
        if not len(self.liste_liens_pcc) == 0:
            for lien in self.liste_liens_pcc:
                self.canvas.create_line(lien.routeurs[0].x + 50, lien.routeurs[0].y + 50, lien.routeurs[1].x + 50,
                                        lien.routeurs[1].y + 50, fill="red", width=4)
            self.print_liens(self.liste_liens_visites, "darkblue")

        """Affiche le cout du graphes comprenant l'ensemble des liens retenus"""
        if not self.cout == 0:
            self.canvas.create_text(10, 0, anchor=NW, font="Times 20 bold", text="Cout: {}".format(self.cout))

        if not self.delai == 0:
            self.canvas.create_text(10, 30, anchor=NW, font="Times 20 bold", text="Delai: {}".format(self.delai))

        if len(self.liste_liens_desactives) != 0:
            for lien in self.liste_liens_desactives:
                self.canvas.create_line(lien.routeurs[0].x + 50, lien.routeurs[0].y + 50, lien.routeurs[1].x + 50,
                                        lien.routeurs[1].y + 50, fill="gray78")
                self.print_liens(self.liste_liens_desactives, "gray78")

        if self.lien_select is not None:
            if self.lien_select.actif:
                self.canvas.create_line(self.lien_select.routeurs[0].x + 50, self.lien_select.routeurs[0].y + 50,
                                        self.lien_select.routeurs[1].x + 50,
                                        self.lien_select.routeurs[1].y + 50, fill="chartreuse3", width=4)
            else:
                self.canvas.create_line(self.lien_select.routeurs[0].x + 50, self.lien_select.routeurs[0].y + 50,
                                        self.lien_select.routeurs[1].x + 50,
                                        self.lien_select.routeurs[1].y + 50, fill="DarkOliveGreen1", width=4)
            self.print_liens([self.lien_select], "blue")

        """Dessine et rempli en gris l'ensemble des routeurs de notre graphe en indiquant au centre le numéro du 
        routeur """
        for routeur in self.liste_routeurs:
            self.canvas.create_oval(routeur.x - 10 + 50, routeur.y - 10 + 50, routeur.x + 10 + 50, routeur.y + 10 + 50,
                                    fill="gray")
            self.canvas.create_text(routeur.x + 50, routeur.y + 50, fill="white", font="Times 10 italic bold",
                                    text=routeur.id)

        """Dessine en rouge le routeur qui a été séléctionné comme routeur d'arrivée"""
        if self.routeur_arrivee_selected:
            self.canvas.create_oval(self.routeur_arrivee.x - 10 + 50, self.routeur_arrivee.y - 10 + 50,
                                    self.routeur_arrivee.x + 10 + 50, self.routeur_arrivee.y + 10 + 50,
                                    fill="red")
            self.canvas.create_text(self.routeur_arrivee.x + 50, self.routeur_arrivee.y + 50, fill="white",
                                    font="Times 10 italic bold",
                                    text=self.routeur_arrivee.id)

        """Dessine en vert le routeur qui a été séléctionné comme routeur de départ"""
        if self.routeur_depart_selected:
            self.canvas.create_oval(self.routeur_depart.x - 10 + 50, self.routeur_depart.y - 10 + 50,
                                    self.routeur_depart.x + 10 + 50, self.routeur_depart.y + 10 + 50,
                                    fill="green")
            self.canvas.create_text(self.routeur_depart.x + 50, self.routeur_depart.y + 50, fill="white",
                                    font="Times 10 italic bold",
                                    text=self.routeur_depart.id)

        if len(self.liste_routeurs_creation_lien) != 0:
            for routeur in self.liste_routeurs_creation_lien:
                self.canvas.create_oval(routeur.x-10+50, routeur.y-10+50,
                                        routeur.x+10+50, routeur.y+10+50,
                                        fill="cyan")
                self.canvas.create_text(routeur.x+50, routeur.y+50, fill="white",
                                        font="Times 10 italic bold",
                                        text=routeur.id)

        self.canvas.create_oval(self.point[0] - 2, self.point[1] - 2, self.point[0] + 2, self.point[1] + 2, fill="red")

    def print_liens(self, liste_liens, couleur):
        for lien in liste_liens:
            min_x = min(lien.routeurs[0].x + 50, lien.routeurs[1].x + 50)
            min_y = min(lien.routeurs[0].y + 50, lien.routeurs[1].y + 50)
            x_middle = min_x + abs(lien.routeurs[0].x - lien.routeurs[1].x) / 2
            y_middle = min_y + abs(lien.routeurs[0].y - lien.routeurs[1].y) / 2
            if self.affiche_delai:
                self.canvas.create_text(x_middle, y_middle, fill=couleur, font="Times 8 italic bold",
                                        text="({};{})".format(lien.cout, lien.delai))
            else:
                self.canvas.create_text(x_middle, y_middle, fill=couleur, font="Times 8 italic bold",
                                        text=lien.cout)

    """Fonction permettant d'exporter un graph en format .txt afin de la recharger par la suite"""

    def exporter(self):
        filename = filedialog.asksaveasfilename(initialdir="/", title="Select file",
                                                filetypes=(("text files", "*.txt"), ("all files", "*.*")))
        self.folder_path.set(filename)
        text = ""
        for routeur in self.liste_routeurs:
            text += str(routeur.id) + "," + str(routeur.x) + "," + str(routeur.y) + ";"
        text += "/"
        for lien in self.liste_liens:
            text += str(lien.id) + "," + str(lien.routeurs[0].id) + "," + str(lien.routeurs[1].id) + "," + str(
                lien.cout) + "," + str(lien.delai) + ";"
        with open(filename + ".txt", "a") as file:
            file.write(text)
        return 0

    """Fonction permettant de charger un graph ayant été préalablement enregistré dans un fichier .txt"""

    def importer(self):
        filename = filedialog.askopenfilename(initialdir="/", title="Selectionner fichier",
                                              filetypes=(("text files", "*.txt"), ("all files", "*.*")))
        self.folder_path.set(filename)
        with open(filename) as file:
            text = file.read()
            routeurs, liens = text.split("/")
            liste_routeurs = routeurs.split(";")
            liste_liens = liens.split(";")
            liste_new_routeurs = []
            liste_new_liens = []
            for routeur in liste_routeurs:
                if not routeur == '':
                    info_routeur = routeur.split(",")
                    new_routeur = Routeur(int(info_routeur[0]), int(info_routeur[1]), int(info_routeur[2]))
                    liste_new_routeurs.append(new_routeur)
            for lien in liste_liens:
                if not lien == '':
                    info_lien = lien.split(",")
                    liste_routeurs_lien = []
                    for routeur in liste_new_routeurs:
                        if routeur.id == int(info_lien[1]) or routeur.id == int(info_lien[2]):
                            liste_routeurs_lien.append(routeur)
                    new_lien = Lien(int(info_lien[0]), liste_routeurs_lien[0], liste_routeurs_lien[1],
                                    int(info_lien[3]),
                                    int(info_lien[4]))
                    liste_new_liens.append(new_lien)
                    for routeur in liste_new_routeurs:
                        if routeur == new_lien.routeurs[0] or routeur == new_lien.routeurs[1]:
                            routeur.liens.append(new_lien)
        self.liste_liens = liste_new_liens
        self.liste_routeurs = liste_new_routeurs
        self.print()

    """Fonction permettant de lancer les differents algorithmes de routage"""

    def start(self):
        self.liste_liens_pcc = []
        self.liste_liens_visites = []
        self.liste_liens_FrankWolfe = []
        self.lien_select = None
        self.cout = 0

        """Lancement de l'agorithme de recouvrement minimum"""
        if self.algorithme_select == 1:
            if self.routeur_depart_selected:
                self.liste_liens_visites, self.cout = prim(30, self.routeur_depart)
                self.print()
            else:
                messagebox.showerror("Routeur non séléctionné", "Vous devez séléctionner un routeur de départ")
        else:

            """Lancement de l'algorithme du plus court chemin sans contraintes"""
            if self.algorithme_select == 2:

                """Si un routeur de départ et un routeur d'arrivée ont été séléctionnés, alors on fait l'algorithme 
                du plus court chemin du routeur de départ au routeur d'arrivée. Si un seul routeur de départ à été 
                séléctionner, alors on fait l'algorithme du plus court chemin entre le routeur de départ et tous les 
                autres routeurs du graph """
                if self.routeur_depart_selected and self.routeur_arrivee_selected:
                    self.liste_liens_visites, self.liste_liens_pcc, self.cout = dijkstra(self.routeur_depart,
                                                                                         self.routeur_arrivee,
                                                                                         False,
                                                                                         0)
                    self.print()
                else:
                    if self.routeur_depart_selected and not self.routeur_arrivee_selected:
                        self.liste_liens_visites, self.liste_liens_pcc, self.cout = dijkstra(
                            self.routeur_depart,
                            self.routeur_arrivee,
                            True,
                            self.nb_routeurs)
                        self.print()
                    else:
                        messagebox.showerror("Routeur non séléctionné",
                                             "Vous devez séléctionner au moins un routeur de départ")
            else:

                """Fonction permettant de lancer l'algorithme de Frank and Wolfe"""
                if self.algorithme_select == 3:
                    if self.routeur_arrivee_selected and self.routeur_arrivee_selected:
                        nouvelle_liste_liens = []
                        for lien in self.liste_liens:
                            if not lien in self.liste_liens_desactives:
                                nouvelle_liste_liens.append(lien)
                        self.liste_liens_FrankWolfe = frankwolfe(self.routeur_depart, self.routeur_arrivee,
                                                                 nouvelle_liste_liens,
                                                                 self.liste_routeurs)
                        self.print()
                else:
                    self.liste_liens_pcc, self.delai, self.cout = dijkstra_contrainte(self.routeur_depart,
                                                                                 self.routeur_arrivee,
                                                                                 int(self.delai_max),
                                                                                 100)
                    if self.liste_liens_pcc[0] == "Impossible" or self.liste_liens_pcc[0] == "Pas assez d'itérations":
                        messagebox.showwarning("Problème avec les données séléctionnées",
                                               self.liste_liens_pcc[0])
                        self.liste_liens_pcc = []
                    else:
                        self.print()

    """Fonction permettant de selectionner l'algorithme de routage que l'on souhaite faire tourner"""

    def choix_algorithme(self, choix):

        if not self.algorithme_select == choix:
            self.liste_liens_pcc = []
            self.liste_liens_visites = []
            self.liste_liens_FrankWolfe = []
            self.cout = 0
            self.affiche_delai = False

        if choix == "Arbre de recouvrement minimum":
            self.algorithme_select = 1
            self.routeur_arrivee = None
            self.routeur_arrivee_selected = False
        else:
            if choix == "Dijkstra":
                self.algorithme_select = 2
            else:
                if choix == "Frank_Wolfe":
                    self.algorithme_select = 3
                else:
                    self.algorithme_select = 4
                    self.affiche_delai = True
        self.print()

    """MouseListener permettant de séléctionner les routeurs de départ et d'arrivée"""

    def callback(self, event):
        x = event.x
        y = event.y
        self.point = [x, y]
        for routeur in self.liste_routeurs:
            if routeur.x + 50 - 10 < x < routeur.x + 50 + 10 and routeur.y + 50 - 10 < y < routeur.y + 50 + 10:
                if self.mode_creation_lien:
                    self.liste_routeurs_creation_lien.append(routeur)
                    if len(self.liste_routeurs_creation_lien) == 2:
                        self.ajout_lien()
                else:
                    if self.algorithme_select == 1:
                        if not self.routeur_depart_selected:

                            self.routeur_depart = routeur
                            self.routeur_depart_selected = True
                        else:
                            if routeur == self.routeur_depart:
                                self.routeur_depart = None
                                self.routeur_depart_selected = False
                            else:
                                self.routeur_depart = routeur
                    else:
                        if not self.routeur_depart_selected:
                            if not routeur == self.routeur_arrivee:
                                self.routeur_depart = routeur
                                self.routeur_depart_selected = True
                            else:
                                self.routeur_arrivee_selected = False
                                self.routeur_arrivee = None
                        else:
                            if self.routeur_depart == routeur:
                                self.routeur_depart_selected = False
                                self.routeur_depart = None
                            else:
                                if not self.routeur_arrivee_selected:
                                    self.routeur_arrivee_selected = True
                                    self.routeur_arrivee = routeur
                                else:
                                    if self.routeur_arrivee == routeur:
                                        self.routeur_arrivee_selected = False
                                        self.routeur_arrivee = None
                                    else:
                                        self.routeur_arrivee = routeur
                self.print()
                return
        ok = True
        for lien in self.liste_liens:
            x1, y1 = lien.routeurs[0].x + 50, lien.routeurs[0].y + 50
            x2, y2 = lien.routeurs[1].x + 50, lien.routeurs[1].y + 50
            if (x1 <= x <= x2 and y1 <= y <= y2) or (x2 <= x <= x1 and y1 <= y <= y2) or (
                    x1 <= x <= x2 and y2 <= y <= y1) or (x2 <= x <= x1 and y2 <= y <= y1):
                if not x1 == x2:
                    a = (y1 - y2) / (x1 - x2)
                    b = y1 - a * x1
                    y3 = a * x + b
                    if y3 - 5 <= y <= y3 + 5:
                        if lien == self.lien_select:
                            self.lien_select = None
                        else:
                            self.lien_select = lien
                            self.modifier_lien()
                            ok = False

                else:
                    if lien == self.lien_select:
                        self.lien_select = None

                    else:
                        self.lien_select = lien
                        ok = False
        if ok:
            self.lien_select = None
        self.print()
        return

    def modifier_lien(self):
        self.label_lien['text'] = "Lien entre les routeurs {} et {}".format(self.lien_select.routeurs[0].id,
                                                                            self.lien_select.routeurs[1].id)
        self.entry_capacite.delete(0, END)
        self.entry_delai.delete(0, END)
        self.entry_capacite.insert(0, str(self.lien_select.cout))
        self.entry_delai.insert(0, str(self.lien_select.delai))
        if self.lien_select.actif:
            self.checkbox_active.select()
        else:
            self.checkbox_active.deselect()

    def setCapacite(self, *args):
        if self.lien_select is not None:
            try:
                for lien in self.liste_liens:
                    if lien == self.lien_select:
                        lien.cout = int(self.var_capacite.get())
                        self.lien_select.cout = int(self.var_capacite.get())
            except:
                d = 0
            self.print()

    def setDelai(self, *args):
        if self.lien_select is not None:
            try:
                for lien in self.liste_liens:
                    if lien == self.lien_select:
                        lien.delai = int(self.var_delai.get())
                        self.lien_select.delai = int(self.var_delai.get())
            except:
                d = 0
            self.print()

    def setActive(self):
        if self.lien_select is not None:
            if self.actif.get() == 0:
                for lien in self.liste_liens:
                    if self.lien_select == lien:
                        lien.actif = False
                        self.lien_select.actif = False
                        lien.routeurs[0].liens.remove(lien)
                        lien.routeurs[1].liens.remove(lien)
                self.liste_liens_desactives.append(self.lien_select)
            else:
                self.liste_liens_desactives.remove(self.lien_select)
                for lien in self.liste_liens:
                    if self.lien_select == lien:
                        lien.actif = True
                        lien.routeurs[0].liens.append(lien)
                        lien.routeurs[1].liens.append(lien)
                        self.lien_select.actif = True
            self.print()

    def creation_lien(self):
        self.liste_routeurs_creation_lien = []
        self.mode_creation_lien = True

    def ajout_lien(self):
        reponse = messagebox.askokcancel("Créer un nouveau lien?","Voulez-vous créer un nouveau lien entre les routeurs {} et {}".format(self.liste_routeurs_creation_lien[0].id, self.liste_routeurs_creation_lien[1].id))
        if reponse:
            id = 0
            exite = False
            for lien in self.liste_liens:
                if lien.id < id:
                    id = lien.id
                if self.liste_routeurs_creation_lien[0] in lien.routeurs and self.liste_routeurs_creation_lien[1] in lien.routeurs:
                    exite = True
            if not exite:
                nouveau_lien = Lien(id+1, self.liste_routeurs_creation_lien[0], self.liste_routeurs_creation_lien[1], randint(0,10), randint(100,1000))
                self.liste_liens.append(nouveau_lien)
                self.liste_routeurs_creation_lien[0].liens.append(nouveau_lien)
                self.liste_routeurs_creation_lien[1].liens.append(nouveau_lien)
            else:
                messagebox.showerror("Impossible de créer le lien", "Attention, le lien existe déjà")
        self.liste_routeurs_creation_lien = []
        self.mode_creation_lien = False

    """Fonction permettant de générer un nouveau graph"""

    def nouveau_graphe(self):
        self.reset()
        self.liste_routeurs, self.liste_liens = creation_graphe(self.nb_routeurs)
        self.print()

    """Fonction permettant de reinitialiser l'ensemble des valeurs de notre interface"""

    def reset(self):
        self.liste_routeurs = []
        self.liste_liens = []
        self.routeur_depart_selected = False
        self.routeur_depart = None
        self.routeur_arrivee_selected = False
        self.routeur_arrivee = None
        self.liste_liens_visites = []
        self.liste_liens_pcc = []
        self.liste_liens_FrankWolfe = []
        self.lien_select = None

    def setNbRouteur(self, nb_routeurs):
        self.nb_routeurs = self.slider_nb_routeurs.get()

    def setDelaiMax(self):
        self.delai_max = self.slider_delay_max.get()
