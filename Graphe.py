from random import randint
from math import *

from Routeur import Routeur
from Lien import Lien


def creation_routeur(nb_routeurs, distance_min, size_x, size_y):
    liste_points = []
    n = 0
    while n != nb_routeurs:
        x = randint(0, size_x)
        y = randint(0, size_y)
        if len(liste_points) != 0:
            distance_check = True
            for point in liste_points:
                if sqrt(pow(x - point[0], 2) + pow(y - point[1], 2)) <= distance_min:
                    distance_check = False
                    break
            if distance_check:
                liste_points.append([x, y])
                n += 1
        else:
            liste_points.append([x, y])
            n += 1
    liste_points = trie_points(liste_points)
    liste_routeurs = []
    i = 1
    for point in liste_points:
        liste_routeurs.append(Routeur(i, point[0], point[1]))
        i += 1
    return liste_routeurs


def trie_points(liste_points):
    if len(liste_points) <= 1:
        return liste_points
    else:
        liste_a = liste_points[:int(len(liste_points) / 2)]
        liste_b = liste_points[int(len(liste_points) / 2 + 1) - 1:int(len(liste_points))]
        return fusion(trie_points(liste_a), trie_points(liste_b))


def fusion(liste_a, liste_b):
    if len(liste_b) == 0:
        return liste_a
    if len(liste_a) == 0:
        return liste_b
    if liste_a[0][0] <= liste_b[0][0]:
        return [liste_a[0]] + fusion(liste_a[1:], liste_b)
    else:
        return [liste_b[0]] + fusion(liste_a, liste_b[1:])


def creation_lien(liste_routeurs, distance_max, nb_max_lien_routeur):
    liste_lien = []
    n = 1
    for routeur_a in liste_routeurs:
        for routeur_b in liste_routeurs:
            if not routeur_a == routeur_b:
                lien_existe = False
                for lien in liste_lien:
                    if (lien.routeurs[0] == routeur_a and lien.routeurs[1] == routeur_b) or (
                            lien.routeurs[0] == routeur_b and lien.routeurs[1] == routeur_a):
                        lien_existe = True
                        break
                if not lien_existe:
                    if sqrt(pow(routeur_a.x - routeur_b.x, 2) + pow(routeur_a.y - routeur_b.y, 2)) <= distance_max:
                        lien = Lien(n, routeur_a, routeur_b, randint(1, 10), randint(100, 1000))
                        liste_lien.append(lien)
                        routeur_a.liens.append(lien)
                        routeur_b.liens.append(lien)
                        n += 1
    return liste_lien


def graphe_connecte(liste_liens, liste_routeurs, nb_routeurs):
    if not len(liste_routeurs) == 0:
        nb_routeurs_connectes = 1
        while not nb_routeurs_connectes == nb_routeurs:
            liste_routeurs_connectes = [liste_routeurs[0]]
            boolean = True
            while boolean:
                n = 0
                liste_liens_connectes = []
                for routeur in liste_routeurs_connectes:
                    liste_liens_connectes = liste_liens_connectes + routeur.liens
                for lien in liste_liens_connectes:
                    if not (lien.routeurs[0] in liste_routeurs_connectes and lien.routeurs[
                        1] in liste_routeurs_connectes):
                        nb_routeurs_connectes += 1
                        n += 1
                        if lien.routeurs[0] in liste_routeurs_connectes:
                            liste_routeurs_connectes.append(lien.routeurs[1])
                        else:
                            liste_routeurs_connectes.append(lien.routeurs[0])
                if n == 0:
                    boolean = False
            nb_routeurs_connectes = len(liste_routeurs_connectes)
            if not nb_routeurs_connectes == nb_routeurs:
                for routeur in liste_routeurs:
                    if not routeur in liste_routeurs_connectes:
                        distance_min = -1
                        routeur_min = None
                        for routeur_candidat in liste_routeurs_connectes:
                            if not routeur_candidat == routeur:
                                distance = sqrt(
                                    pow(routeur_candidat.x - routeur.x, 2) + pow(routeur_candidat.y - routeur.y,
                                                                                 2))
                                if distance_min > 0:
                                    if distance < distance_min:
                                        distance_min = distance
                                        routeur_min = routeur_candidat
                                else:
                                    distance_min = distance
                                    routeur_min = routeur_candidat
                        nouveau_lien = Lien(len(liste_liens) + 1, routeur_min, routeur, randint(1, 10),
                                            randint(100, 1000))
                        routeur_min.liens.append(nouveau_lien)
                        routeur.liens.append(nouveau_lien)
                        liste_liens.append(nouveau_lien)
                        nb_routeurs_connectes += 1
                        break
    return liste_liens


def creation_graphe(nb_routeurs):
    liste_routeurs = creation_routeur(nb_routeurs, 70, 1000, 500)
    liste_liens = creation_lien(liste_routeurs, 175, 4)
    graphe_connecte(liste_liens, liste_routeurs, nb_routeurs)
    return liste_routeurs, liste_liens
